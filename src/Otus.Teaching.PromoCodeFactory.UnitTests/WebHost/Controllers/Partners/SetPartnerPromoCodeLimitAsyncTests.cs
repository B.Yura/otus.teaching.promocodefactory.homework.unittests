﻿using AutoFixture.AutoMoq;
using AutoFixture;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System.Threading.Tasks;
using Xunit;
using System;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly PartnersController _partnersController;
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }
        public Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            //Arange
            var partnerId = Guid.Parse("53477be3-45ff-465c-9a7c-4255f4c555fc");
            Partner partner = null;
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partner);
            var request = new Fixture().Build<SetPartnerPromoCodeLimitRequest>().Create();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();


        }
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            //Arange
            var partner = CreateBasePartner();
            partner.IsActive= false;
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            var request = new Fixture().Build<SetPartnerPromoCodeLimitRequest>().Create();

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request) as BadRequestObjectResult;

            //Assert
            result.Should().NotBeNull();
            result.Value.Should().Be("Данный партнер не активен");

        }
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_NumberIssuedPromoCodes_EqualsZero()
        {
            //Arange
            var partner = CreateBasePartner();
            partner.NumberIssuedPromoCodes =0;
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            var request = new Fixture().Build<SetPartnerPromoCodeLimitRequest>().Create();

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request) as CreatedAtActionResult;

            //Assert
            var model = result.Value as Partner;
            model.Should().NotBeNull();
            model.NumberIssuedPromoCodes.Should().Be(0);

        }
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_Cancel_PreviousLimit()
        {
            //Arange
            var partner = CreateBasePartner();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            var request = new Fixture().Build<SetPartnerPromoCodeLimitRequest>().Create();

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request) as CreatedAtActionResult;

            //Assert
            var model = result.Value as Partner;
            model.Should().NotBeNull();
            model.PartnerLimits.FirstOrDefault(x => x.CancelDate.HasValue).Should().NotBeNull();

        }
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_Limit_MoreThanZero()
        {
            //Arange
            var partner = CreateBasePartner();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            var request = new Fixture().Build<SetPartnerPromoCodeLimitRequest>().Create();
            request.Limit=0;
            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request) as BadRequestObjectResult;

            //Assert
            result.Should().NotBeNull();
            result.Value.Should().Be("Лимит должен быть больше 0");

        }
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_AddNewLimit_ShouldBeAdded()
        {
            //Arange
            var partner = CreateBasePartner();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
            var request = new Fixture().Build<SetPartnerPromoCodeLimitRequest>().Create();

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request) as CreatedAtActionResult;
            //Assert
            result.Should().NotBeNull();

            var model = result.Value as Partner;
            model.Should().NotBeNull();

            model.PartnerLimits.Should().HaveCount(x => x >1);

        }
    }
}